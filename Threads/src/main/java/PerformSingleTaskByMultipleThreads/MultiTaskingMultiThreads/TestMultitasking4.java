package PerformSingleTaskByMultipleThreads.MultiTaskingMultiThreads;

public class TestMultitasking4 {
    public static void main(String[] args) {

        Thread t1 = new Thread() {
            public void run() {
                System.out.println("task one");
            }
        };
        Thread t2 = new Thread() {
            public void run() {
                System.out.println("task two");
            }
        };
        t1.start();
        t2.start();

        Runnable r1 = new Runnable() {
            public void run() {
                System.out.println("task 3");
            }
        };

        Runnable r2 = new Runnable() {
            public void run() {
                System.out.println("task 4");
            }
        };

        Thread t3 = new Thread(r1);
        Thread t4 = new Thread(r2);

        t3.start();
        t4.start();
    }
}
