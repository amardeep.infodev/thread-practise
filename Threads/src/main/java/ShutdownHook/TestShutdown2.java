package ShutdownHook;

public class TestShutdown2{
    public static void main(String[] args) throws Exception{
        Runtime r = Runtime.getRuntime();
//        r.addShutdownHook(new MyThread());
//        r.addShutdownHook(new Thread());

//        System.out.println("Now main sleeping..press ctrl+c to exit");
//        try{
//            Thread.sleep(3000);
//        }
//        catch (Exception e){
//        }
        try {
            RemoveHookExample.Msg ms = new RemoveHookExample.Msg();
//            Thread.sleep(3000);
            r.addShutdownHook(ms);
            System.out.println("Start");

            System.out.println("Waiting 3 seconds");
            Thread.sleep(2000);

            r.removeShutdownHook(ms);
            System.out.println("Program terminating");
        }
        catch (Exception ex){
            ex.printStackTrace();
        }


    }
}

